let osc = require('osc'),
    express = require('express'),
    ws = require('ws');

let config = {
    local_port: 53000,
    remote_port: 53000,
    osc_passcode: process.env.OSC_PASSCODE,
    debug: process.env.DEBUG
}

function Qlab2WS(config) {
    let self = this;

    self.config = config;
}

Qlab2WS.prototype.init = function () {
    let self = this;

    // Bind to a TCP socket to listen for incoming OSC events.
    self.oscPort = new osc.TCPSocketPort({
        localAddress: "0.0.0.0",
        localPort: self.config.local_port,
        address: "host.docker.internal",
        port: self.config.remote_port,
        metadata: true
    });


    self.oscPort.on("ready", function () {
        console.log("Listening for OSC on port " + self.config.local_port);
    });

    self.oscPort.on("error", function (err) {
        console.log(err);
    });

    self.oscPort.on("message", function (oscMessage) {
        if (self.config.debug) {
            console.log('Recieved via OSC Socket: %s', oscMessage);
        }
    });

    self.oscPort.open();
    self.init_osc();

    setInterval(function(){self.init_osc();}, 1000*50)

    // Create an Express-based Web Socket server to which OSC messages will be relayed.
    let appResources = __dirname + "/web",
        app = express(),
        server = app.listen(8081),
        wss = new ws.Server({
            server: server
        });
    console.log("Listening on ws://localhost:8081 for websocket connections.");

    app.use("/", express.static(appResources));
    wss.on("connection", function (socket) {
        console.log("A Web Socket connection has been established!");

        self.oscPort.on("message", function (oscMessage) {
            socket.send(JSON.stringify(oscMessage))
            if (self.config.debug) {
                console.log('Sent OSC to Websocket: %s', oscMessage)
            }
        })

        socket.on('message', function message(data) {
            let s = data.toString()
            let message = JSON.parse(s)
            self.oscPort.send(message)
            if (self.config.debug) {
                console.log('Recieved OSC Message from Websocket: %s', message)
            }
        });

    });

}

Qlab2WS.prototype.init_osc = function () {
    let self = this;

    self.oscPort.send({
        address: "/connect",
        args: [
            {
                type: "s",
                value: self.config.osc_passcode
            }
        ]
    });

    self.oscPort.send({
        address: "/updates",
        args: [
            {
                type: "i",
                value: 1
            }
        ]
    });
}

qlab2ws = new Qlab2WS(config);
qlab2ws.init()
