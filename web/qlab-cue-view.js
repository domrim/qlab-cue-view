function QLabCueView(){

}

QLabCueView = function () {
    var self = this

    self.colors = colors
    self.debug = true

    self.resetVars()

    self.socket = new WebSocket('ws://localhost:8081')
};

QLabCueView.prototype.qCueRequest = [
    {
        type: 's',
        value: JSON.stringify(["number","uniqueID","listName","type","isPaused","duration","actionElapsed","parent","flagged","notes","autoLoad","colorName","isRunning","isLoaded","armed","isBroken","percentActionElapsed","cartPosition","infiniteLoop","holdLastFrame"])
    }
];

QLabCueView.prototype.init = function () {
    let self = this

    self.listen();

    // Collect required DOM Elements
    self.elems = {
        next: document.getElementById("next"),
        running: document.getElementById("running"),
        hours: document.getElementById("hours"),
        minutes: document.getElementById("minutes"),
        seconds: document.getElementById("seconds"),
        duration: document.getElementById("duration")
    }

    self.prime_vars();
}


QLabCueView.prototype.listen = function () {
    let self = this;

    self.socket.onmessage = function (event) {
        let message = JSON.parse(event.data)

        if (message.address.match(/^\/update\//)) {
            self.readUpdate(message);
        } else if (message.address.match(/^\/reply\//)) {
            self.readReply(message);
        } else {
            if (self.debug) {console.log(message)}
        }
    }
};

QLabCueView.prototype.sendOSC = function (address, args = []) {
    let self = this;

    self.socket.send(
        JSON.stringify({
            address: address,
            args: args
        })
    )
}

QLabCueView.prototype.resetVars = function (doUpdate) {
    let self = this;
    let qName = '';
    let qNum = '';
    let qID = '';
    let cues = self.wsCues;

    // play head info
    self.nextCue = '';
    // most recent running cue
    self.runningCue = new Cue();

    // clear 'variables'
    if (doUpdate) {
        self.updateNextCue();
        //self.updatePlaying();
    }

    self.wsCues = {};
    self.cueColors = {};
    self.cueOrder = [];
    self.cueByNum = {};
    self.cueList = {};
    self.requestedCues = {};

    self.timerStart = 0;
}

// get current status of QLab cues and playhead
// and ask for updates
QLabCueView.prototype.prime_vars = function () {
    let self = this;

    console.log("Run prime_vars")

    if (self.needWorkspace && self.ready) {
        self.sendOSC("/version", []);		// app global, not workspace

        // request variable/feedback info
        // get list of running cues
        self.sendOSC("/cue/playhead/uniqueID", []);
        self.sendOSC("/cueLists", []);
        self.sendOSC("/auditionWindow",[]);
        self.sendOSC("/overrideWindow",[]);
        self.sendOSC("/showMode",[]);
        self.sendOSC("/settings/general/minGoTime");
        if (self.timer !== undefined) {
            clearTimeout(self.timer);
            self.timer = undefined;
        }
        self.timer = setTimeout(function () { self.prime_vars(); }, 5000);
    }
};

QLabCueView.prototype.updateNextCue = function () {
    let self = this;
    let nc = self.wsCues[self.nextCue];
    if (!nc) {
        nc = new Cue();
    }

    self.elems.next.innerHTML = (nc.uniqueID ? nc.qName : ' ')

    /*
    self.setVariable('n_id', nc.uniqueID);
    self.setVariable('n_name', nc.qName);
    self.setVariable('n_num', nc.qNumber);
    self.setVariable('n_type', nc.qType);
    self.setVariable('n_notes', nc.Notes);
    self.setVariable('n_stat', nc.isBroken ? self.QSTATUS_CHAR.broken :
        nc.isRunning ? self.QSTATUS_CHAR.running :
            nc.isPaused ? self.QSTATUS_CHAR.paused :
                nc.isLoaded ? self.QSTATUS_CHAR.loaded :
                    self.QSTATUS_CHAR.idle);
     */
};


QLabCueView.prototype.readUpdate = function (message) {
    let self = this;
    let ma = message.address
    let mf = ma.split('/');

    if (ma.match(/playbackPosition$/)) {
        let cl = ma.substring(63, 63 + 36)
        if (message.args.length > 0) {
            let oa = message.args[0].value;
            // if a cue is inserted, QLab sends playback changed cue
            // before sending the new cue's id update, insert this id into
            // the cuelist just in case so the playhead check will find it until then
            if (!self.cueList[cl].includes(oa)) {
                self.cueList[cl].push(oa);
            }
            if (oa !== self.nextCue) {
                // playhead changed
                self.nextCue = oa;
                self.sendOSC("/cue_id/" + oa + "/valuesForKeys", self.qCueRequest);
                self.requestedCues[oa] = Date.now();
            }
        } else {
            // no playhead
            self.nextCue = '';
            self.updateNextCue();
        }
    } else if (ma.match(/cue lists]$/)) {
        //self.sendOSC('/doubleGoWindowRemaining')
    } else if (ma.match(/\/cue_id\//)) {
        // get cue information for 'updated' cue
        let node = ma.substring(7) + "/valuesForKeys";
        let uniqueID = ma.slice(-36);
        self.sendOSC(node, self.qCueRequest);
        // save info request time to verify a response.
        // QLab sends an update when a cue is deleted
        // but fails to respond to a request for info.
        // If there is no response after a few pulses
        // we delete our copy of the cue

        self.requestedCues[uniqueID] = Date.now();
    } else {
        if (self.debug) {console.log(message)}
    }
}

QLabCueView.prototype.readReply = function (message) {
    let self = this;
    let ma = message.address;
    let uniqueID

    let j = JSON.parse(message.args[0].value);

    if (ma.match(/valuesForKeys$/)) {
        self.updateCues(j.data, 'v');
        uniqueID = ma.substring(14, 14 + 36);
        delete self.requestedCues[uniqueID];
    } else if (ma.match(/children$/)) {
        if (j.data) {
            uniqueID = ma.substring(14, 14 + 36);
            self.updateCues(j.data, 'u', uniqueID);
        }
    } else {
        if (self.debug) {console.log(message)}
    }
}

QLabCueView.prototype.updateCues = function (jCue, stat, ql) {
    let self = this;
    // list of useful cue types we're interested in
    let qTypes = ['audio', 'mic', 'video', 'camera',
        'text', 'light', 'fade', 'network', 'midi', 'midi file',
        'timecode', 'group', 'start', 'stop', 'pause', 'load',
        'reset', 'devamp', 'goto', 'target', 'cart', 'cue list',
        'arm', 'disarm', 'wait', 'memo', 'script'
    ];
    let q = {};

    if (Array.isArray(jCue)) {
        let i = 0;
        let idCount = {};
        let dupIds = false;
        while (i < jCue.length) {
            q = new Cue(jCue[i],self);
            q.qOrder = i;
            if (ql) {
                q.qList = ql;
            }
            if (stat === 'u') {
                if (!self.cueList[ql].includes(q.uniqueID)) {
                    self.cueList[ql].push(q.uniqueID);
                }
            } else {
                if (q.uniqueID in idCount) {
                    idCount[q.uniqueID] += 1;
                    dupIds = true;
                } else {
                    idCount[q.uniqueID] = 1;
                }

                if (qTypes.includes(q.qType)) {
                    self.updateQVars(q);
                    self.wsCues[q.uniqueID] = q;
                }
                if (stat === 'l') {
                    self.cueOrder[i] = q.uniqueID;
                    if (ql) {
                        self.cueList[ql].push(q.uniqueID);
                    }
                }
            }
            delete self.requestedCues[q.uniqueID];
            i += 1;
        }
    } else {
        q = new Cue(jCue,self);
        if (qTypes.includes(q.qType)) {
            self.updateQVars(q);
            self.wsCues[q.uniqueID] = q;
            // a 'cart' is a special 'cue list'
            if (['cue list','cart'].includes(q.qType)) {
                if (!self.cueList[q.uniqueID]) {
                    self.cueList[q.uniqueID] = [];
                } else {
                    self.sendOSC("/cue_id/" + q.uniqueID + "/children",[]);
                }
            }
            self.updatePlaying();
            if (q.uniqueID === self.nextCue) {
                self.updateNextCue();
            }
        }
        delete self.requestedCues[q.uniqueID];
    }
}

QLabCueView.prototype.updateQVars = function (q) {
    let self = this;
    let qID = q.uniqueID;
    let qNum = (q.qNumber).replace(/[^\w\.]/gi, '_');
    let qType = q.qType;
    let qColor = q.qColor;
    let oqNum = null;
    let oqName = null;
    let oqType = null;
    let oqColor = 0;
    let oqOrder = -1;

    // unset old variable?
    if (qID in self.wsCues) {
        oqNum = self.wsCues[qID].qNumber.replace(/[^\w\.]/gi,'_');
        oqName = self.wsCues[qID].qName;
        oqType = self.wsCues[qID].qType;
        oqColor = self.wsCues[qID].qColor;
        oqOrder = self.wsCues[qID].qOrder;
        if (oqNum !== '' && oqNum !== q.qNumber) {
            // self.setVariable('q_' + oqNum + '_name');
            self.cueColors[oqNum] = 0;
            delete self.cueByNum[oqNum];
            oqName = '';
        }
    }
    // set new value
    if (q.qName !== oqName || qColor !== oqColor) {
        if (qNum !== '') {
            // self.setVariable('q_' + qNum + '_name', q.qName);
            self.cueColors[qNum] = q.qColor;
            self.cueByNum[qNum] = qID;
        }
        // self.setVariable('id_' + qID + '_name', q.qName);
    }
};

/**
 * update list of running cues
 */
QLabCueView.prototype.updatePlaying = function () {
    function qState (q) {
        let ret = q.uniqueID + ':';
        ret +=
            q.isBroken ? '0' :
                q.isRunning ? '1' :
                    q.isPaused ? '2' :
                        q.isLoaded ? '3' :
                            '4';
        ret += ":" + q.duration + ":" + q.pctElapsed;
        return ret;
    }

    let self = this;
    let hasGroup = false;
    let hasDuration = false;
    let i;
    let cues = self.wsCues;
    let lastRun = qState(self.runningCue);
    let runningCues = [];
    let q;

    Object.keys(cues).forEach(function (cue) {
        q = cues[cue];
        // some cuelists (for example all manual slides) may not have a pre-programmed duration
        if (q.isRunning || q.isPaused) {
            if (('cue list' !== q.qType) || (self.cueList[""] && self.cueList[""].includes(cue))) {
                runningCues.push([cue, q.startedAt]);
                // if group does not have a duration, ignore
                // it is probably a playlist, not simultaneous playback
                hasGroup = hasGroup || (q.qType === "group" && q.duration > 0);
                hasDuration = hasDuration || q.duration > 0;
            }
        }
    });

    runningCues.sort(function (a, b) {
        return b[1] - a[1];
    });

    if (runningCues.length === 0) {
        self.runningCue = new Cue();
    } else {
        i = 0;
        if (hasGroup) {
            while (i < runningCues.length && cues[runningCues[i][0]].qType !== "group") {
                i += 1;
            }
        } else if (hasDuration) {
            while (i < runningCues.length && cues[runningCues[i][0]].duration === 0) {
                i += 1;
            }
        }
        if (i < runningCues.length) {
            self.runningCue = cues[runningCues[i][0]];
            // to reduce network traffic, the query interval logic only asks for running 'updates'
            // if the playback elapsed is > 0%. Sometimes, the first status response of a new running cue
            // is exactly when the cue starts, with 0% elapsed and the countdown timer won't run.
            // Set a new cue with 0% value to 1 here to cause at least one more query to see if the cue is
            // actually playing.
            if (0 === self.runningCue.pctElapsed) {
                self.runningCue.pctElapsed = 1;
            }
        }
    }
    // update if changed
    if (qState(self.runningCue) !== lastRun) {
        self.updateRunning(true);
    }
};

QLabCueView.prototype.updateRunning = function () {
    let self = this;
    let tenths = 1;
    let rc = self.runningCue;

    let tElapsed = rc.duration * rc.pctElapsed;

    let timeElapsed = seconds2time(tElapsed)
    let ehh = timeElapsed.hh;
    let emm = timeElapsed.mm;
    let ess = timeElapsed.ss;
    let eft = '';

    if (ehh > 0) {
        eft = ehh + ":";
    }
    if (emm > 0) {
        eft = eft + emm + ":";
    }
    eft = eft + ess;

    let tLeft = rc.duration * (1 - rc.pctElapsed);
    if (tLeft > 0) {
        tLeft += tenths;
    }

    let timeLeft = seconds2time(tLeft);
    let hh = timeLeft.hh;
    let mm = timeLeft.mm;
    let ss = timeLeft.ss;
    let ft = '';

    if (hh > 0) {
        ft = hh + ":";
    }
    if (mm > 0) {
        ft = ft + mm + ":";
    }
    ft = ft + ss;

    if (tenths === 0) {
        let f = Math.floor((tLeft - Math.trunc(tLeft)) * 10);
        let ms = ('0' + f).slice(-1);
        if (tLeft < 5 && tLeft !== 0) {
            ft = ft.slice(-1) + "." + ms;
        }
    }

    let offset = Math.floor(tElapsed) * 1000
    self.timerStart = Date.now() - offset;

    self.elems.hours.innerHTML = ehh
    self.elems.minutes.innerHTML = emm
    self.elems.seconds.innerHTML = ess

    if (rc.isRunning) {
        self.interval = setInterval(function(){self.runTimer();}, 200)
        self.elems.running.innerHTML = rc.qName

        let duration = seconds2time(rc.duration)
        self.elems.duration.innerHTML = duration.hh + ':' + duration.mm + ':' + duration.ss
    } else {
        clearInterval(self.interval)
        self.elems.running.innerHTML = ' '
        self.elems.duration.innerHTML = ' '
    }

};

QLabCueView.prototype.runTimer = function () {
    let self = this;
    let rc = self.runningCue;

    if (!rc.isRunning) {
        clearInterval(self.interval)
        return
    }

    let delta = Date.now() - self.timerStart;

    let elapsedSeconds = Math.floor(delta / 1000)

    let formatedTime = seconds2time(elapsedSeconds)

    self.elems.hours.innerHTML = formatedTime.hh
    self.elems.minutes.innerHTML = formatedTime.mm
    self.elems.seconds.innerHTML = formatedTime.ss
}

function seconds2time(seconds){
    let eh = Math.floor(seconds / 3600);
    let ehh = ('00' + eh).slice(-2);
    let em = Math.floor(seconds / 60) % 60;
    let emm = ('00' + em).slice(-2);
    let es = Math.floor(seconds % 60);
    let ess = ('00' + es).slice(-2);

    return {
        hh: ehh,
        mm: emm,
        ss: ess,
    }
}
