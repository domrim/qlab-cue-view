FROM node:18-bullseye

WORKDIR /src/app

COPY package*.json ./

RUN npm install

COPY . .

EXPOSE 8081

CMD [ "node", "index.js" ]