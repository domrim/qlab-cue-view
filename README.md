# QLab Cue View

At the "[Die lange Nacht der kurzen Stücke – Grenzenlos](https://www.kulturzentrum-tempel.de/index.php?id=1729)" I needed a way to display the time of the running cue from QLab to an external display for the lighting technician.
So this "piece" of software was created during a few night shifts.

## How it works

The server part exists of a http and a websocket server. The websocket server translates messages from/to OSC to/from Websocket. The website (also served by the websocket api server) displays the current cue running time, the name of the cue, its duration and the name of the next cue.

## Future work

ATM most of the "inteligence" and state is stored in the web-side of the app, it might be a good idea for more robustness of the app, to hold the state of the running cues and so on inside the serverside.

## Credits

Inspiration & some lines of code was leant/taken from:
  * [osc.js-examples - UDP -> Web Socket Example](https://github.com/colinbdclark/osc.js-examples/tree/master/browser)
  * [companion-module-figure53-qlab-advance](https://github.com/bitfocus/companion-module-figure53-qlab-advance)

